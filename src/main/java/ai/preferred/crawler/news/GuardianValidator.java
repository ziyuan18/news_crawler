package ai.preferred.crawler.news;

import ai.preferred.venom.request.Request;
import ai.preferred.venom.response.Response;
import ai.preferred.venom.validator.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;

public enum GuardianValidator implements Validator {

    INSTANCE;
    private static final Logger LOGGER = LoggerFactory.getLogger(GuardianValidator.class);


    @Override
    public Status isValid(@NotNull Request request, @NotNull Response response) {
        if(response.getStatusCode() != 200){
            return Status.INVALID_CONTENT;
        }
        return Status.VALID;
    }
}
