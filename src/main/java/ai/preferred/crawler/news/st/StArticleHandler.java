package ai.preferred.crawler.news.st;

import ai.preferred.crawler.news.entity.SelfParsingArticle;
import ai.preferred.crawler.news.reuters.ReutersArticleHandler;
import ai.preferred.minerva.IEntityStorage;
import ai.preferred.venom.Handler;
import ai.preferred.venom.Session;
import ai.preferred.venom.Worker;
import ai.preferred.venom.job.Scheduler;
import ai.preferred.venom.request.Request;
import ai.preferred.venom.response.VResponse;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static ai.preferred.crawler.news.NewsCrawlerApp.AbstractCrawler.MONGO_MANAGER_KEY;

public class StArticleHandler implements Handler {
    private static final Logger LOGGER = LoggerFactory.getLogger(StArticleHandler.class);

    @Override
    public void handle(Request request, VResponse vResponse, Scheduler scheduler, Session session, Worker worker) {
        Document doc = vResponse.getJsoup();
        final IEntityStorage storage = session.get(MONGO_MANAGER_KEY);

        final SelfParsingArticle.StParser parser = new SelfParsingArticle.StParser(doc, request.getUrl());
        LOGGER.info("Saving... {}", request.getUrl());
        worker.submit(() -> storage.insertOrUpdate(parser.parse(), request.getUrl()));
    }
}
