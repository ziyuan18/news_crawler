package ai.preferred.crawler.news.st;

import ai.preferred.crawler.news.NewsCrawlerApp;
import ai.preferred.venom.Crawler;
import ai.preferred.venom.request.VRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StCrawler extends NewsCrawlerApp.AbstractCrawler{
    private static final Logger LOGGER = LoggerFactory.getLogger(StCrawler.class);

    @Override
    protected void crawl() throws Exception {
        try (final Crawler crawler = getCrawler().start()) {
            final String[] topics = {"asia", "world", "politics", "tech", "business", "singapore", "sport", "lifestyle"};
            for(String topic : topics){
                crawler.getScheduler().add(new VRequest("https://www.straitstimes.com/"+ topic + "/latest"), new StListingHandler());
                for(int i = 1; i < 5; i++){
                    crawler.getScheduler().add(new VRequest("https://www.straitstimes.com/"+ topic + "/latest?page=" + i), new StListingHandler());
                }
            }
//            crawler.getScheduler().add(new VRequest("https://www.straitstimes.com/singapore/latest?page=1"), new StListingHandler());
        }
    }

    public static void main(String[] args) {
        NewsCrawlerApp.runAndExit(StCrawler.class, args);
    }
}
