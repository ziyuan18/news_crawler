package ai.preferred.crawler.news.st;

import ai.preferred.venom.Handler;
import ai.preferred.venom.Session;
import ai.preferred.venom.Worker;
import ai.preferred.venom.job.Scheduler;
import ai.preferred.venom.request.Request;
import ai.preferred.venom.request.VRequest;
import ai.preferred.venom.response.VResponse;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StListingHandler implements Handler {
    private static final Logger LOGGER = LoggerFactory.getLogger(StListingHandler.class);

    @Override
    public void handle(Request request, VResponse vResponse, Scheduler scheduler, Session session, Worker worker) {
        Document doc = vResponse.getJsoup();
        Element list_pane = doc.selectFirst(".pane-content .view-content");
        Elements media_list = list_pane.select(".media");
        for(Element e : media_list){
            final String art_link = e.selectFirst("a").attr("href");
            if(art_link.equals("//str.sg/whatispremium")){
//                LOGGER.info("Paid Link: {}", e.selectFirst("a").attr("href"));
            } else {
//                LOGGER.info("Scheduling Document: {}", art_link);
                scheduler.add(new VRequest("https://www.straitstimes.com" + art_link), new StArticleHandler());
            }

        }
    }
}
