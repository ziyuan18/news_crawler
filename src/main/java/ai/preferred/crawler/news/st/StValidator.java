package ai.preferred.crawler.news.st;

import ai.preferred.venom.request.Request;
import ai.preferred.venom.response.Response;
import ai.preferred.venom.response.VResponse;
import ai.preferred.venom.validator.Validator;

import javax.validation.constraints.NotNull;

public enum StValidator implements Validator {
    ;

    @Override
    public Status isValid(@NotNull Request request, @NotNull Response response) {
        if(response.getStatusCode() != 200){
            return Status.INVALID_CONTENT;
        }
        final VResponse responseWrapped = new VResponse(response);
        final String html = responseWrapped.getHtml();
        return Status.VALID;
    }
}
