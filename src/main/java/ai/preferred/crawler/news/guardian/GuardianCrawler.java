package ai.preferred.crawler.news.guardian;

import ai.preferred.crawler.news.NewsCrawlerApp;
import ai.preferred.venom.Crawler;
import ai.preferred.venom.request.VRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.HashMap;

public class GuardianCrawler extends NewsCrawlerApp.AbstractCrawler{
    private static final Logger LOGGER = LoggerFactory.getLogger(GuardianCrawler.class);

    @Override
    protected void crawl() throws Exception {
        try (final Crawler crawler = getCrawler().start()) {
            final String[] topics = {"football", "sport", "politics", "technology", "business", "environment", "education", "global-development", "society", "culture"};
            final String[] months = {"jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"};
            final HashMap<String, Integer> noDaysInMonth = new HashMap<>();
            LocalDate today = LocalDate.now();
            final int year = today.getYear();
            noDaysInMonth.put("jan", 31);
            noDaysInMonth.put("feb", 28);
            noDaysInMonth.put("mar", 31);
            noDaysInMonth.put("apr", 30);
            noDaysInMonth.put("may", 31);
            noDaysInMonth.put("jun", 30);
            noDaysInMonth.put("jul", 31);
            noDaysInMonth.put("aug", 31);
            noDaysInMonth.put("sep", 30);
            noDaysInMonth.put("oct", 31);
            noDaysInMonth.put("nov", 30);
            noDaysInMonth.put("dec", 31);
            final int month = today.getMonthValue() - 1;
            final int day = today.getDayOfMonth();
            String m = months[month];
            final String yr = Integer.toString(year);
            for(String topic : topics) {
                for (int i = day; i > (day - 3); i--){
                    if(i < 1){
                        final int roll_month = month - 1;
                        final String str_roll_month = roll_month == -1 ? "dec" : months[roll_month];
                        final String str_roll_year = roll_month == -1 ? Integer.toString(year - 1) : yr;
                        final int ndim = noDaysInMonth.get(str_roll_month);
                        final int j = ndim + i;
                        crawler.getScheduler().add(new VRequest("https://www.theguardian.com/"+ topic + "/" + str_roll_year + "/" + str_roll_month + "/" + j + "/all"), new GuardianListingHandler());
                    } else {
                        if(i < 10){
                            final String padded_i = "0" + Integer.toString(i);
                            crawler.getScheduler().add(new VRequest("https://www.theguardian.com/"+ topic + "/" + yr + "/" + m + "/" + padded_i + "/all"), new GuardianListingHandler());
                        } else {
                            crawler.getScheduler().add(new VRequest("https://www.theguardian.com/"+ topic + "/" + yr + "/" + m + "/" + i + "/all"), new GuardianListingHandler());
                        }
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        NewsCrawlerApp.runAndExit(GuardianCrawler.class, args);
    }
}
