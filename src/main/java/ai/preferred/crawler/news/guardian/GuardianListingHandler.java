package ai.preferred.crawler.news.guardian;

import ai.preferred.venom.Handler;
import ai.preferred.venom.Session;
import ai.preferred.venom.Worker;
import ai.preferred.venom.job.Scheduler;
import ai.preferred.venom.request.Request;
import ai.preferred.venom.request.VRequest;
import ai.preferred.venom.response.VResponse;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GuardianListingHandler implements Handler {
    private static final Logger LOGGER = LoggerFactory.getLogger(GuardianListingHandler.class);

    @Override
    public void handle(Request request, VResponse vResponse, Scheduler scheduler, Session session, Worker worker) {
        Document doc = vResponse.getJsoup();

        Elements brexits = doc.select(".fc-item__container > .js-headline-text");
        for(Element bx : brexits){
            String art_url = bx.attr("href");
            scheduler.add(new VRequest(art_url), new GuardianArticleHandler());
        }
    }
}
