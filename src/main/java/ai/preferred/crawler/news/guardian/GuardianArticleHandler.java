package ai.preferred.crawler.news.guardian;

import ai.preferred.crawler.news.entity.SelfParsingArticle;
import ai.preferred.minerva.IEntityStorage;
import ai.preferred.venom.Handler;
import ai.preferred.venom.Session;
import ai.preferred.venom.Worker;
import ai.preferred.venom.job.Scheduler;
import ai.preferred.venom.request.Request;
import ai.preferred.venom.response.VResponse;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static ai.preferred.crawler.news.NewsCrawlerApp.AbstractCrawler.MONGO_MANAGER_KEY;

public class GuardianArticleHandler implements Handler {
    private static final Logger LOGGER = LoggerFactory.getLogger(GuardianArticleHandler.class);

    @Override
    public void handle(Request request, VResponse vResponse, Scheduler scheduler, Session session, Worker worker) {
        Document doc = vResponse.getJsoup();
        final IEntityStorage storage = session.get(MONGO_MANAGER_KEY);
        final SelfParsingArticle.GuardianParser parser = new SelfParsingArticle.GuardianParser(doc, request.getUrl());
        LOGGER.info("Saving... {}", request.getUrl());
        worker.submit(() -> storage.insertOrUpdate(parser.parse(), request.getUrl()));
    }
}
