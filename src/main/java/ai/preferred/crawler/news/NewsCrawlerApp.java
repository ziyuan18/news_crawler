package ai.preferred.crawler.news;

import ai.preferred.crawler.news.mindreaderutils.BowGenerator;
import ai.preferred.crawler.news.mindreaderutils.MatrixOperator;
import ai.preferred.minerva.DummyEntityStorage;
import ai.preferred.minerva.EntityStorage;
import ai.preferred.minerva.IEntityStorage;
import ai.preferred.venom.Crawler;
import ai.preferred.venom.Handler;
import ai.preferred.venom.Session;
import ai.preferred.venom.SleepScheduler;
import ai.preferred.venom.fetcher.AsyncFetcher;
import ai.preferred.venom.job.LazyScheduler;
import ai.preferred.venom.proxy.ProxiedAsyncFetcher;
import ai.preferred.venom.proxy.ProxyReactorUtils;
import ai.preferred.venom.proxy.logger.Logger;
import ai.preferred.venom.proxy.provider.ProxyReactor;
import ai.preferred.venom.proxy.provider.RatingProxyProvider;
import ai.preferred.venom.request.Request;
import ai.preferred.venom.storage.FileManager;
import ai.preferred.venom.validator.EmptyContentValidator;
import ai.preferred.venom.validator.PipelineValidator;
import ai.preferred.venom.validator.Validator;
import com.google.common.collect.ImmutableSet;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.mongodb.MongoClient;
import org.kohsuke.args4j.CmdLineParser;

import java.io.Closeable;
import java.io.IOException;
import java.util.*;

public class NewsCrawlerApp {

    private static Module[] modules = {
            new NewsServerModule()
    };

    public static void setModule(Module module) {
        NewsCrawlerApp.modules = new Module[]{
                module
        };
    }

    public abstract static class AbstractCrawler implements Closeable {

        public static final Session.Key<IEntityStorage> MONGO_MANAGER_KEY = new Session.Key<>();

        @Inject
        private Provider<MongoClient> mongoProvider;

        @Inject
        private Provider<FileManager> fileManagerProvider;

        @Inject(optional = true)
        @Named("debug")
        private boolean debug = false;

        @Inject(optional = true)
        @Named("mongoDB")
        private String db;

        @Inject(optional = true)
        @Named("maxConnections")
        private int maxConnections = Runtime.getRuntime().availableProcessors() * 128;

        @Inject(optional = true)
        @Named("connectionRequestTimeout")
        private int connectionRequestTimeout = 60000;

        @Inject(optional = true)
        @Named("connectTimeout")
        private int connectTimeout = 60000;

        @Inject(optional = true)
        @Named("socketTimeout")
        private int socketTimeout = 60000;

        private MongoClient mongo;
        private FileManager fileManager;
        private Session session;

        public AbstractCrawler() {
        }

        protected EntityStorage getStorage() {
            return new EntityStorage(mongo.getDatabase(db));
        }

        protected FileManager getFileManager() {
            return fileManager;
        }

        protected Session getSession() {
            return session;
        }

        protected Crawler getCrawlerWithEmptySession() {
            return getCrawler(Session.EMPTY_SESSION, null, null, null);
        }

        protected Crawler getCrawler(Session session) {
            return getCrawler(session, null, null, GuardianValidator.INSTANCE);
        }

        protected Crawler getCrawler() {
            return getCrawler(null, null);
        }

        protected Crawler getCrawler(Iterator<Request> iter, Handler h) {
            return getCrawler(session, iter, h, GuardianValidator.INSTANCE);
        }

        protected Crawler getCrawler(Iterator<Request> iter, Handler h, Validator validator) {
            return getCrawler(session, iter, h, validator);
        }

        private Crawler getCrawler(Session session, Iterator<Request> iter, Handler h, Validator validator) {
            final Set<Integer> ports = ImmutableSet.of(3128, 443, 53281, 63909, 65301, 65309, 80, 8080, 8118);
            final RatingProxyProvider pp = new RatingProxyProvider();
            Logger.scheduleLogging(pp);

            final ProxyReactor reactor = ProxyReactorUtils.getDefaultProxyReactor(ports);

            final List<Validator> validatorList = new ArrayList<>(Arrays.asList(
                    EmptyContentValidator.INSTANCE
            ));

            if (validator != null) {
                validatorList.add(validator);
            }

            final ProxiedAsyncFetcher fetcher = ProxiedAsyncFetcher.builder(
                    AsyncFetcher.builder()
                            .setConnectTimeout(connectTimeout)
                            .setSocketTimeout(socketTimeout)
                            .setFileManager(getFileManager())
                            .disableCookies()
                            .setMaxConnections(maxConnections)
                            .setValidator(new PipelineValidator(validatorList)))
                    .setProxyProvider(pp)
                    .setProxyReactor(reactor)
                    .build();

            final Crawler.Builder builder = Crawler.builder()
                    .setSession(session)
                    .setFetcher(fetcher)
                    .setMaxConnections(maxConnections)
                    .setSleepScheduler(new SleepScheduler(0, 5));

            if (iter != null && h != null) {
                builder.setScheduler(new LazyScheduler(iter, h));
            }

            return builder.build();
        }

        private void start(String[] args) throws Exception {
            new CmdLineParser(this).parseArgument(args);
//            fileManager = fileManagerProvider.get();
            mongo = mongoProvider.get();
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                if (mongo != null) {
                    mongo.close();
                }


//                if (fileManager != null) {
//                    try {
//                        fileManager.close();
//                    } catch (Exception e) {
//                        throw new RuntimeException(e);
//                    }
//                }

            }));
            final IEntityStorage storage;
            if (!debug) {
                storage = getStorage();
            } else {
                storage = new DummyEntityStorage();
            }

            session = Session.builder().put(MONGO_MANAGER_KEY, storage).build();
            if (!BowGenerator.INSTANCE.init()){
                throw new Exception("unable to load params");
            }
            MatrixOperator.INSTANCE.init();
            crawl();
        }

        protected abstract void crawl() throws Exception;

        @Override
        public void close() throws IOException {
            if (mongo != null) {
                mongo.close();
            }
            /*if (fileManager != null && fileManager instanceof Closeable) {
                ((Closeable) fileManager).close();
            }*/
        }

    }

    private static <T extends AbstractCrawler> T getCrawler(Class<T> clazz) {
        return Guice.createInjector(modules).getInstance(clazz);
    }

    public static int run(Class<? extends AbstractCrawler> clazz, String[] args) {
        try (final AbstractCrawler crawler = getCrawler(clazz)) {
            crawler.start(args);
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
        return 0;
    }

    /**
     * Terminates the currently running JVM, when crawler terminates either with or without exception.
     *
     * @param clazz the crawler application class
     * @param args  the app arguments
     */
    public static void runAndExit(Class<? extends AbstractCrawler> clazz, String[] args) {
        System.exit(run(clazz, args));
    }

}