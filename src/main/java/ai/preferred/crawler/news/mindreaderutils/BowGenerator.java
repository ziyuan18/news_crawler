package ai.preferred.crawler.news.mindreaderutils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.sql.*;
import java.util.*;

public enum BowGenerator {

    INSTANCE;

    private final List<String> corpus = new ArrayList<>();
    private final List<String> w = new ArrayList<>();
    private final List<String> b = new ArrayList<>();

    private Random random = new Random(System.currentTimeMillis());

    private static final Logger LOGGER = LoggerFactory.getLogger(BowGenerator.class);

    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;

    public double[][] getW(){
        int x_len = w.size();
        int y_len = w.get(0).split(" ").length;
        double[][] result = new double[x_len][y_len];
        for(int i = 0; i < x_len; i++){
            String[] encoder_w_strrow = w.get(i).split(" ");
            double[] encoder_w_drow = new double[encoder_w_strrow.length];
            for(int j = 0; j < encoder_w_strrow.length; j++){
                encoder_w_drow[j] = Double.parseDouble(encoder_w_strrow[j]);
            }
            result[i] = encoder_w_drow;
        }

        return result;
    }

    public double[] getB(){
        double[] result = new double[b.size()];
        for(int i = 0; i < b.size(); i++){
            result[i] = Double.parseDouble(b.get(i));
        }
        return result;
    }

    public String[] getVOC(){
        String[] result = new String[corpus.size()];
        result = corpus.toArray(result);
        return result;
    }

    public double[] getBOW(List<String> tokens){
        double[] bow = new double[corpus.size()];
        for(int i = 0; i < corpus.size(); i++){
            bow[i] = Collections.frequency(tokens, corpus.get(i));
        }
        return bow;
    }

    public boolean init(){
        try {
            InputStream input = BowGenerator.class.getResourceAsStream("/guardian.properties");

            Properties prop = new Properties();

            // load a properties file
            prop.load(input);

            // get the property value and print it out
            String mysqlhost = prop.getProperty("pysqlHost");
            String mysqluser = prop.getProperty("pysqlUser");
            String mysqlpassword = prop.getProperty("pysqlPassword");

            // This will load the MySQL driver, each DB has its own driver
            Class.forName("com.mysql.jdbc.Driver");
            // Setup the connection with the DB
            connect = DriverManager
                    .getConnection(mysqlhost, mysqluser, mysqlpassword);

            List<String> params = getParams();
            String[] voc = params.get(0).split(" ");
            String[] encoder_w_uncutrows = params.get(1).split("NNNN");
            String[] encoder_b = params.get(2).split(" ");
            LOGGER.info("VOC: {}", params.get(0));
            corpus.addAll(Arrays.asList(voc));
            w.addAll(Arrays.asList(encoder_w_uncutrows));
            b.addAll(Arrays.asList(encoder_b));
        } catch (Exception e) {
            LOGGER.error("BowGenerator: Error Retrieving Model Parameters:", e);
            return false;
        } finally {
            close();
        }
        return true;
    }
    //voc, param_w, param_b
    private List<String> getParams() throws SQLException {
        List<String> result = new ArrayList<>();
        preparedStatement = connect.prepareStatement("select * from artstore.sparams where pid = ?");
        preparedStatement.setInt(1, 1);
        resultSet = preparedStatement.executeQuery();
        resultSet.first();
        String voc = resultSet.getString("voc");
        String encoder = resultSet.getString("encoder");
        String bias = resultSet.getString("bias");
        //LOGGER.info(resultSet.getString("voc"));
        //LOGGER.info(resultSet.getString("encoder"));
        //LOGGER.info(resultSet.getString("bias"));
        result.add(voc);
        result.add(encoder);
        result.add(bias);
        return result;
    }

    private void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {

        }
    }
}
