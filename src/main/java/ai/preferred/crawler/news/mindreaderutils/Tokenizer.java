package ai.preferred.crawler.news.mindreaderutils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class Tokenizer {
    // list of english language stop words (mostly extracted from nltk)
    final static String[] stoppers = {"t", "ve", "on", "were", "all", "haven", "yourselves", "that'll", "won't", "until", "me", "have", "her", "mustn't", "don", "you'd", "theirs", "to", "such", "yours", "you've", "not", "through", "is", "weren", "his", "from", "off", "very", "being", "of", "above", "didn't", "hadn't", "do", "should've", "for", "its", "so", "in", "than", "further", "mustn", "as", "any", "where", "am", "ll", "who", "what", "here", "did", "our", "haven't", "we", "o", "doing", "and", "isn't", "you", "how", "m", "hadn", "under", "hasn", "which", "only", "are", "ours", "s", "again", "himself", "itself", "having", "into", "weren't", "couldn't", "down", "i", "while", "by", "doesn't", "after", "yourself", "now", "shouldn't", "some", "when", "was", "each", "that", "out", "because", "him", "been", "these", "isn", "hers", "y", "mightn't", "ourselves", "myself", "with", "mightn", "there", "re", "no", "once", "against", "aren", "herself", "both", "own", "wasn", "couldn", "she", "shouldn", "it", "needn", "they", "you'll", "said","those", "wouldn", "would", "my", "d", "she's", "does", "the", "wouldn't", "or", "more", "nor", "before", "won", "an", "other", "their", "doesn", "whom", "this", "can", "had", "be", "over", "below", "but", "has", "hasn't", "during", "between", "wasn't", "don't", "them", "at", "he", "didn", "it's", "few", "why", "needn't", "shan't", "too", "about", "up", "most", "themselves", "same", "just", "will", "you're", "ain", "ma", "shan", "if", "your", "a", "should", "then", "aren't"};

    private static final Logger LOGGER = LoggerFactory.getLogger(Tokenizer.class);

    public static ArrayList<String> stopTokenizer(String[] input){
        List<String> stops = Arrays.asList(stoppers);
        ArrayList<String> stoppedtokens = new ArrayList<String>();
        int i = 0;
        for(String s : input){
            if(!stops.contains(s)){
                //LOGGER.info(s);
                if(i < 4){
                    if(s.equals(s.toUpperCase())){
                        //LOGGER.info("removed: {}", s);
                        continue;
                    }
                    if(s.equals("Reuters")){
                        //LOGGER.info("removed: {}", s);
                        continue;
                    }
                    if(s.contains("Reuters")){
                        //LOGGER.info("removed: {}", s);
                        continue;
                    }
                }
                String lower_s = s.toLowerCase();
                if(!stops.contains(lower_s)){
                    String[] strary = lower_s.split("\\P{Alpha}+");
                    stoppedtokens.addAll(Arrays.asList(strary));
                }
            }
            i++;
        }
        stoppedtokens.removeIf(n -> (n.length() < 2));
        return stoppedtokens;
    }
}
