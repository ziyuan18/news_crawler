package ai.preferred.crawler.news.mindreaderutils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public enum MatrixOperator {
    INSTANCE;

    private final double[][] W = BowGenerator.INSTANCE.getW();
    private final double[] B = BowGenerator.INSTANCE.getB();
    private final String[] voc = BowGenerator.INSTANCE.getVOC();

    private static final Logger LOGGER = LoggerFactory.getLogger(MatrixOperator.class);

    public double[][] getW(){
        return W;
    }

    public double[] getB(){
        return B;
    }

    public String[] getVOC(){
        return voc;
    }

    public double[] arradd(double[] A){
        double[] result = new double[B.length];
        for(int i=0 ; i < B.length; i++){
            result[i] = A[i] + B[i];
        }
        return result;
    }

    public double[] matmul(double[] Ax/*, Double[][] B*/) {
        double[][] A = new double[1][Ax.length];
        double[][] B = getW();
        A[0] = Ax;
        int aRows = A.length;
        int aColumns = A[0].length;
        int bRows = B.length;
        int bColumns = B[0].length;

        if (aColumns != bRows) {
            throw new IllegalArgumentException("A:Rows: " + aColumns + " did not match B:Columns " + bRows + ".");
        }

        double[][] C = new double[aRows][bColumns];
        for (int i = 0; i < aRows; i++) {
            for (int j = 0; j < bColumns; j++) {
                C[i][j] = 0.00000;
            }
        }

        for (int i = 0; i < aRows; i++) { // aRow
            for (int j = 0; j < bColumns; j++) { // bColumn
                for (int k = 0; k < aColumns; k++) { // aColumn
                    C[i][j] += A[i][k] * B[k][j];
                }
            }
        }

//        LOGGER.info(Arrays.deepToString(C));
        return C[0];
    }

    public boolean init(){
//        System.out.print("2020-07-08 23:17:36 INFO  BowGenerator:79 - VOC: ");
//        for(String v : voc){
//            System.out.print(v + " ");
//        }
//        System.out.println();
//        System.out.print("2020-07-08 23:17:36 INFO  BowGenerator:79 - VOC: ");
//        for(double[] i : W){
//            for(double j : i){
//                System.out.print(j + " ");
//            }
//            System.out.println();
//        }
//        LOGGER.info("Length of VOC: {}", voc.length);
//        LOGGER.info("Length of W: {}", W.length);
//        LOGGER.info("Length of W1: {}", W[0].length);
//        LOGGER.info("Length of B: {}", B.length);
        LOGGER.info("MatrixOperator Params are Up!");
        return true;
    }
}
