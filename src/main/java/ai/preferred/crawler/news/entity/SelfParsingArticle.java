package ai.preferred.crawler.news.entity;

import ai.preferred.crawler.news.mindreaderutils.BowGenerator;
import ai.preferred.crawler.news.mindreaderutils.MatrixOperator;
import ai.preferred.crawler.news.mindreaderutils.Tokenizer;
import ai.preferred.minerva.Entity;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SelfParsingArticle extends Entity<String> {

    public static final String COLLECTION = "articles";

    private static final Logger LOGGER = LoggerFactory.getLogger(SelfParsingArticle.class);

    private String articleId;
    private String author;
    private Date date;
    private String content;
    private List<String> tokens;
    private String title;
    private String section;
    private String url;
    private String embed;
    private String publisher;
    private String description;

    private SelfParsingArticle(String articleId){
        super(articleId);
    }

    // Straits Times Parser
    public static class StParser {
        private static final Logger LOGGER = LoggerFactory.getLogger(StParser.class);

        private static final String ISO_8601_24H_FULL_FORMAT = "yyyy-MM-dd'T'HH:mm:ssX";

        private final Document doc;
        private final String url;

        public StParser(Document doc, String url){
            this.doc = doc;
            this.url = url;
        }

        public SelfParsingArticle parse() {
            final SelfParsingArticle article = new SelfParsingArticle(parseArticleId());
            article.url = parseURL();
            article.author = parseAuthor();
            article.title = parseTitle();
            article.section = parseSection();
            article.date = parseDate();
            article.tokens = parseTokens();
            article.content = parseContent();
            article.publisher = "The Straits Times";
            article.embed = getEmbed(article.tokens);
            article.description = parseDescription();
            LOGGER.info(article.tokens.toArray().toString());

            return article;
        }

        private String parseArticleId(){
            Date qn = parseDate();
            String[] tns = parseTitle().split("\\P{Alpha}+");
            String tn = "";
            for (String s : tns){
                tn += s.substring(0,1);
            }
            String suffix = tn.toLowerCase();
            LOGGER.info("TIME: {}", qn.getTime());
            String dn = Long.toString(qn.getTime());
            final String articleId = "S" + dn.substring(0,9) + suffix.substring(0,5);
            LOGGER.info("Article ID: {}", articleId);
            return articleId;
        }

        private String parseURL(){
            return doc.selectFirst("meta[property='og:url']").attr("content");
        }

        private String parseAuthor(){
            return doc.selectFirst("meta[name='article:author']").attr("content");
        }

        private String parseTitle(){
            return doc.selectFirst("meta[property='og:title']").attr("content");
        }

        private String parseSection(){
            final String sect = doc.selectFirst("h2.web-category-name").text();
            return sect.equals("") ? "Uncategorized" : sect;
        }

        private String parseDescription(){
            final String raw_select = doc.selectFirst("meta[name='description']").attr("content");
            final String final_select = raw_select.replaceAll("Read more at straitstimes.com.", "");
            return final_select;
        }

        private Date parseDate(){
            final String raw_date = doc.selectFirst("meta[property='article:published_time']").attr("content");
//            LOGGER.info("Raw Date: {}", raw_date);
            final SimpleDateFormat sdf = new SimpleDateFormat(ISO_8601_24H_FULL_FORMAT);
            try{
                Date d = sdf.parse(raw_date);
                return d;
            } catch (ParseException e) {
                LOGGER.error("Error Parsing Date: {}", e);
            }
            return null;
        }

        private List<String> parseTokens(){
            final Elements art_ps = doc.select(".field-items p");
            final ArrayList<String> bow_input = new ArrayList<>();
            String raw_text = "";
            for(Element e : art_ps){
                String pstext = e.text();
                raw_text += pstext + " ";
            }
            bow_input.addAll(Tokenizer.stopTokenizer(raw_text.split(" ")));
//            LOGGER.info("No. of Tokens: {}", bow_input.size());
            return bow_input;
        }

        private String parseContent(){
            final Elements art_ps = doc.select(".field-items p");
            String raw_text = "";
            for(Element e : art_ps){
                String pstext = e.text();
                raw_text += "<p>" + pstext + "</p>";
            }
//            LOGGER.info("Raw Text: {}", raw_text);
            return raw_text;
        }

        private String getEmbed(List<String> tokens){
            double[] bow = BowGenerator.INSTANCE.getBOW(tokens);
            double[] embed = MatrixOperator.INSTANCE.arradd(MatrixOperator.INSTANCE.matmul(bow));
            String result = "";
            for(double d : embed){
                result += Double.toString(d) + " ";
            }
            result = result.substring(0, result.length() -1);
            return result;
        }

    }

    public static class GuardianParser {
        private static final Logger LOGGER = LoggerFactory.getLogger(GuardianParser.class);

        private static final String ISO_8601_24H_FULL_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";

        private final Document doc;
        private final String url;

        public GuardianParser(Document doc, String url){
            this.doc = doc;
            this.url = url;
        }

        public SelfParsingArticle parse() {
            final SelfParsingArticle article = new SelfParsingArticle(parseArticleId());
            article.url = parseURL();
            article.author = parseAuthor();
            article.title = parseTitle();
            article.section = parseSection();
            article.date = parseDate();
            article.tokens = parseTokens();
            article.content = parseContent();
            article.publisher = "The Guardian";
            article.embed = getEmbed(article.tokens);
            article.description = parseDescription();

            return article;
        }

        private String parseArticleId(){
            Date qn = parseDate();
            String[] tns = parseTitle().split("\\P{Alpha}+");
            String tn = "";
            for (String s : tns){
                tn += s.substring(0,1);
            }
            String suffix = tn.toLowerCase();
//            LOGGER.info("TIME: {}", qn.getTime());
            String dn = Long.toString(qn.getTime());
            final String articleId = "G" + dn.substring(0,9) + suffix.substring(0,5);
//            LOGGER.info("Article ID: {}", articleId);
            return articleId;
        }

        private String parseURL(){
            return doc.selectFirst("meta[property='og:url']").attr("content");
        }

        private String parseAuthor(){
            return doc.selectFirst("meta[name='author']").attr("content");
        }

        private String parseTitle(){
            return doc.selectFirst("meta[property='og:title']").attr("content");
        }

        private String parseSection(){
            return doc.selectFirst("meta[property='article:section']").attr("content");
        }

        private String parseDescription(){
            return doc.selectFirst("meta[name='description']").attr("content");
        }

        private Date parseDate(){
            final String raw_date = doc.selectFirst("meta[property='article:published_time']").attr("content");
//            LOGGER.info("Raw Date: {}", raw_date);
            final SimpleDateFormat sdf = new SimpleDateFormat(ISO_8601_24H_FULL_FORMAT);
            try{
                Date d = sdf.parse(raw_date);
                return d;
            } catch (ParseException e) {
                LOGGER.error("Error Parsing Date: {}", e);
            }
            return null;
        }

        private List<String> parseTokens(){
            final Element id_div = doc.selectFirst("div > .content__article-body");
            final Elements art_ps = id_div.select("p");
            final ArrayList<String> bow_input = new ArrayList<>();
            String raw_text = "";
            for(Element e : art_ps){
                String pstext = e.text();
                raw_text += pstext + " ";
            }
            bow_input.addAll(Tokenizer.stopTokenizer(raw_text.split(" ")));
            return bow_input;
        }

        private String parseContent(){
            final Element id_div = doc.selectFirst("div > .content__article-body");
            final Elements art_ps = id_div.select("p");
            String raw_text = "";
            for(Element e : art_ps){
                String pstext = e.text();
                raw_text += "<p>" + pstext + "</p>";
            }
//            LOGGER.info("Raw Text: {}", raw_text);
            return raw_text;
        }

        private String getEmbed(List<String> tokens){
            double[] bow = BowGenerator.INSTANCE.getBOW(tokens);
            double[] embed = MatrixOperator.INSTANCE.arradd(MatrixOperator.INSTANCE.matmul(bow));
            String result = "";
            for(double d : embed){
                result += Double.toString(d) + " ";
            }
            result = result.substring(0, result.length() -1);
            return result;
        }
    }

    public static class ReutersParser {

        private static final Logger LOGGER = LoggerFactory.getLogger(ReutersParser.class);

        private static final String ISO_8601_24H_FULL_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";

        private final Document doc;
        private final String url;

        public ReutersParser(Document doc, String url){
            this.doc = doc;
            this.url = url;
        }

        public SelfParsingArticle parse() {
            final SelfParsingArticle article = new SelfParsingArticle(parseArticleId());
            article.url = parseURL();
            article.author = parseAuthor();
            article.title = parseTitle();
            article.section = parseSection();
            article.date = parseDate();
            article.tokens = parseTokens();
            article.content = parseContent();
            article.publisher = "Reuters";
            article.embed = getEmbed(article.tokens);
            article.description = parseDescription();

            return article;
        }

        private String parseArticleId(){
            final String articleId = "R" + doc.selectFirst("div > .StandardArticle_inner-container").attr("id");
//            LOGGER.info("Article ID: {}", articleId);
            return articleId;
        }

        private String parseAuthor(){
            return doc.selectFirst("meta[property='og:article:author']").attr("content");
        }

        private String parseTitle(){
            return doc.selectFirst("meta[property='og:title']").attr("content");
        }

        private String parseURL(){
            return doc.selectFirst("meta[property='og:url']").attr("content");
        }

        private String parseSection(){
            return doc.selectFirst("meta[property='og:article:section']").attr("content");
        }

        private String parseDescription(){
            return doc.selectFirst("meta[name='description']").attr("content");
        }

        private Date parseDate(){
            final String raw_date = doc.selectFirst("meta[property='og:article:published_time']").attr("content");
//            LOGGER.info("Raw Date: {}", raw_date);
            final SimpleDateFormat sdf = new SimpleDateFormat(ISO_8601_24H_FULL_FORMAT);
            try{
                Date d = sdf.parse(raw_date);
                return d;
            } catch (ParseException e) {
                LOGGER.error("Error Parsing Date: {}", e);
            }
            return null;
        }

        private List<String> parseTokens(){
            final Element id_div = doc.selectFirst("div > .StandardArticle_inner-container");
            final Elements art_ps = id_div.select(".StandardArticleBody_body > p");
            final ArrayList<String> bow_input = new ArrayList<>();
            String raw_text = "";
            for(Element e : art_ps){
                String pstext = e.text();
                raw_text += pstext + " ";
            }
            bow_input.addAll(Tokenizer.stopTokenizer(raw_text.split(" ")));
            return bow_input;
        }

        private String parseContent(){
            final Element id_div = doc.selectFirst("div > .StandardArticle_inner-container");
            final Elements art_ps = id_div.select(".StandardArticleBody_body > p");
            String raw_text = "";
            for(Element e : art_ps){
                String pstext = e.text();
                raw_text += "<p>" + pstext + "</p>";
            }
//            LOGGER.info("Raw Text: {}", raw_text);
            return raw_text;
        }

        private String getEmbed(List<String> tokens){
            double[] bow = BowGenerator.INSTANCE.getBOW(tokens);
            double[] embed = MatrixOperator.INSTANCE.arradd(MatrixOperator.INSTANCE.matmul(bow));
            String result = "";
            for(double d : embed){
                result += Double.toString(d) + " ";
            }
            result = result.substring(0, result.length() -1);
            return result;
        }
    }

    public static class BloombergParser {

        private static final Logger LOGGER = LoggerFactory.getLogger(BloombergParser.class);

        private static final String ISO_8601_24H_FULL_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";

        private final Document doc;
        private final String url;

        public BloombergParser(Document doc, String url){
            this.doc = doc;
            this.url = url;
        }

        public SelfParsingArticle parse() {
            final SelfParsingArticle article = new SelfParsingArticle(parseArticleId());
            article.url = parseURL();
            article.author = parseAuthor();
            article.title = parseTitle();
            article.section = parseSection();
            article.date = parseDate();
            article.tokens = parseTokens();
            article.content = parseContent();
            article.publisher = "Reuters";
            article.embed = getEmbed(article.tokens);
            article.description = parseDescription();

            return article;
        }

        private String parseArticleId(){
            final String articleId = "R" + doc.selectFirst("div > .StandardArticle_inner-container").attr("id");
            LOGGER.info("Article ID: {}", articleId);
            return articleId;
        }

        private String parseAuthor(){
            return doc.selectFirst("meta[property='og:article:author']").attr("content");
        }

        private String parseTitle(){
            return doc.selectFirst("meta[property='og:title']").attr("content");
        }

        private String parseURL(){
            return doc.selectFirst("meta[property='og:url']").attr("content");
        }

        private String parseSection(){
            return doc.selectFirst("meta[property='og:article:section']").attr("content");
        }

        private String parseDescription(){
            return doc.selectFirst("meta[name='description']").attr("content");
        }

        private Date parseDate(){
            final String raw_date = doc.selectFirst("meta[property='og:article:published_time']").attr("content");
//            LOGGER.info("Raw Date: {}", raw_date);
            final SimpleDateFormat sdf = new SimpleDateFormat(ISO_8601_24H_FULL_FORMAT);
            try{
                Date d = sdf.parse(raw_date);
                return d;
            } catch (ParseException e) {
                LOGGER.error("Error Parsing Date: {}", e);
            }
            return null;
        }

        private List<String> parseTokens(){
            final Element id_div = doc.selectFirst("div > .StandardArticle_inner-container");
            final Elements art_ps = id_div.select(".StandardArticleBody_body > p");
            final ArrayList<String> bow_input = new ArrayList<>();
            String raw_text = "";
            for(Element e : art_ps){
                String pstext = e.text();
                raw_text += pstext + " ";
            }
            bow_input.addAll(Tokenizer.stopTokenizer(raw_text.split(" ")));
            return bow_input;
        }

        private String parseContent(){
            final Element id_div = doc.selectFirst("div > .StandardArticle_inner-container");
            final Elements art_ps = id_div.select(".StandardArticleBody_body > p");
            String raw_text = "";
            for(Element e : art_ps){
                String pstext = e.text();
                raw_text += "<p>" + pstext + "</p>";
            }
//            LOGGER.info("Raw Text: {}", raw_text);
            return raw_text;
        }

        private String getEmbed(List<String> tokens){
            double[] bow = BowGenerator.INSTANCE.getBOW(tokens);
            double[] embed = MatrixOperator.INSTANCE.arradd(MatrixOperator.INSTANCE.matmul(bow));
            String result = "";
            for(double d : embed){
                result += Double.toString(d) + " ";
            }
            result = result.substring(0, result.length() -1);
            return result;
        }
    }



}
