package ai.preferred.crawler.news;

import ai.preferred.venom.storage.DummyFileManager;
import ai.preferred.venom.storage.FileManager;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import com.mongodb.MongoClient;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class NewsTestModule extends AbstractModule {
    @Provides
    MongoClient provideMongoDB(@Named("mongoHost") String host, @Named("mongoPort") int port) {
        return new MongoClient(host, port);
    }

    @Provides
    FileManager provideFileManager(@Named("storageDir") String dir) {
        return new DummyFileManager(dir);
    }

    @Override
    protected void configure() {
        try {
            final Properties properties = new Properties();
            properties.setProperty("mongoPort", "27017");
            properties.setProperty("mongoHost", "localhost");
            final InputStream stream = NewsTestModule.class.getResourceAsStream("/guardian.properties");
            properties.load(stream);
            Names.bindProperties(binder(), properties);
        } catch (IOException e) {
            throw new RuntimeException("Unable to load properties", e);
        }
    }

}
