package ai.preferred.crawler.news.reuters;

import ai.preferred.crawler.news.entity.SelfParsingArticle;
import ai.preferred.minerva.IEntityStorage;
import ai.preferred.venom.Handler;
import ai.preferred.venom.Session;
import ai.preferred.venom.Worker;
import ai.preferred.venom.job.Scheduler;
import ai.preferred.venom.request.Request;
import ai.preferred.venom.response.VResponse;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static ai.preferred.crawler.news.NewsCrawlerApp.AbstractCrawler.MONGO_MANAGER_KEY;

public class ReutersArticleHandler implements Handler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReutersArticleHandler.class);

    @Override
    public void handle(Request request, VResponse vResponse, Scheduler scheduler, Session session, Worker worker) {
        final Document doc = vResponse.getJsoup();
//        Element id_div = doc.selectFirst("div > .StandardArticle_inner-container");
        final IEntityStorage storage = session.get(MONGO_MANAGER_KEY);

//        final String author = doc.selectFirst("meta[property='og:article:author']").attr("content");
//        LOGGER.info("Author: {}", author); // author
//        LOGGER.info(doc.selectFirst("meta[property='og:url']").attr("content")); // article url
//        LOGGER.info(doc.selectFirst("meta[property='og:title']").attr("content")); // article title
//        LOGGER.info(doc.selectFirst("meta[property='og:article:section']").attr("content")); // section label for adj-enc
//        LOGGER.info(doc.selectFirst("meta[property='og:type']").attr("content")); // check whether is article
//        LOGGER.info(doc.selectFirst("meta[property='og:article:published_time']").attr("content"));
//        Elements art_ps = id_div.select(".StandardArticleBody_body > p");
//        ArrayList<String> bow_input = new ArrayList<>();
//        ArrayList<String> raw_paras = new ArrayList<>();
//        String raw_text = "";
//        for(Element e : art_ps){
//            String pstext = e.text();
//            raw_paras.add(pstext);
//            raw_text += pstext + " ";
//        }
//        bow_input.addAll(Tokenizer.stopTokenizer(raw_text.split(" ")));
//
//        for(String s: bow_input){
//            System.out.print(s + ";");
//        }
//        System.out.println();
//
//        for(String s : raw_paras){
//            LOGGER.info(s);
//        }

        final SelfParsingArticle.ReutersParser parser = new SelfParsingArticle.ReutersParser(doc, request.getUrl());
        LOGGER.info("Saving... {}", request.getUrl());
        worker.submit(() -> storage.insertOrUpdate(parser.parse(), request.getUrl()));
    }
}
