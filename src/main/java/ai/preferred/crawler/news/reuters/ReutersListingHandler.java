package ai.preferred.crawler.news.reuters;

import ai.preferred.venom.Handler;
import ai.preferred.venom.Session;
import ai.preferred.venom.Worker;
import ai.preferred.venom.job.Scheduler;
import ai.preferred.venom.request.Request;
import ai.preferred.venom.request.VRequest;
import ai.preferred.venom.response.VResponse;
import org.jsoup.nodes.*;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReutersListingHandler implements Handler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReutersListingHandler.class);

    @Override
    public void handle(Request request, VResponse vResponse, Scheduler scheduler, Session session, Worker worker) {
        final Document doc = vResponse.getJsoup();
        Elements story_divs = doc.select(".news-headline-list > article.story:not(.no-border-bottom)");
        for (Element e : story_divs){
//            LOGGER.info(e.select(".story-title").text());
            String href = e.select(".story-content a").first().attr("href");
//            LOGGER.info(href);
            scheduler.add(new VRequest("https://www.reuters.com" + href), new ReutersArticleHandler());
        }
    }
}
