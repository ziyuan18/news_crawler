package ai.preferred.crawler.news.reuters;

import ai.preferred.crawler.news.NewsCrawlerApp;
import ai.preferred.venom.Crawler;
import ai.preferred.venom.request.VRequest;

public class ReutersCrawler extends NewsCrawlerApp.AbstractCrawler{
    @Override
    protected void crawl() throws Exception {
        final String[] topics = {"sportsnews", "aerospace-defense", "sciencenews", "businessnews", "politicsnews", "technologynews"};
        try (final Crawler crawler = getCrawler().start()) {
            for(int i = 1; i < 15; i++){
                crawler.getScheduler().add(new VRequest("https://www.reuters.com/news/archive?view=page&page=" + i + "&pageSize=10"), new ReutersListingHandler());
            }
            for (String tp : topics){
                for(int i = 1; i < 10; i++){
                    crawler.getScheduler().add(new VRequest("https://www.reuters.com/news/archive/" + tp + "?view=page&page=" + i + "&pageSize=10"), new ReutersListingHandler());
                }
            }
        }
    }

    public static void main(String[] args) {
        NewsCrawlerApp.runAndExit(ReutersCrawler.class, args);
    }
}
