package ai.preferred.crawler.news.iter;

import ai.preferred.itertools.CloseableIter;
import ai.preferred.venom.request.Request;

import java.util.NoSuchElementException;

/**
 * @author mtkachenko
 */
public abstract class RequestIterator<T> implements CloseableIter<Request> {

    private final CloseableIter<T> cursor;
    private boolean closed = false;

    public RequestIterator(CloseableIter<T> cursor) {
        this.cursor = cursor;
    }

    protected abstract Request nextRequest(T e);

    @Override
    public final boolean hasNext() {
        if (closed) {
            return false;
        }
        final boolean hasNext = cursor.hasNext();
        if (!hasNext) {
            close();
        }
        return hasNext;
    }

    @Override
    public final Request next() {
        if (closed) {
            throw new NoSuchElementException("MongoDB connection is closed");
        }
        return nextRequest(cursor.next());
    }

    @Override
    public void close() {
        cursor.close();
        closed = true;
    }

}
