package ai.preferred.crawler.news;

import ai.preferred.venom.storage.FileManager;
import ai.preferred.venom.storage.MysqlFileManager;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class NewsServerModule extends AbstractModule {
    @Provides
    MongoClient provideMongoDB(@Named("mongoHost") String host, @Named("mongoPort") String port, @Named("mongoAuthDB") String authDB, @Named("mongoUser") String username, @Named("mongoPassword") String password) {
        final MongoClientOptions options = MongoClientOptions.builder()
                .applicationName("Amazon Crawler")
                .build();
        return new MongoClient(new ServerAddress(host, Integer.parseInt(port)), MongoCredential.createScramSha1Credential(username, authDB, password.toCharArray()), options);
    }

    @Provides
    FileManager provideFileManager(@Named("mysqlHost") String host, @Named("mysqlTable") String table, @Named("mysqlUser") String user, @Named("mysqlPassword") String password, @Named("storageDir") String dir) {
        return new MysqlFileManager(host, table, user, password, dir);
    }

    @Override
    protected void configure() {
        try {
            final Properties properties = new Properties();
            final InputStream stream = NewsTestModule.class.getResourceAsStream("/guardian.properties");
            properties.load(stream);
            Names.bindProperties(binder(), properties);
        } catch (IOException e) {
            throw new RuntimeException("Unable to load properties", e);
        }
    }

}
